#####
# Benötigte Bibliotheken: pyperclip (pip install pyperclip)
#####


from random import randint, choice
from pyperclip import copy
from time import sleep 

LoremIpsum = ["lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipisci", "elit", "Morbi", "sollicitudin", "tortor", "et", "velit", "venenatis", "molestie", "morbi", 
"non", "nibh", "magna", "quis", "tempor", "metus", "Vivamus", "vehicula", "velit", "sit", "amet", "neque", "posuere", "id", "hendrerit", "sem", "Nam", "vitae", "felis", "sem", 
"Mauris", "ultricies", "congue", "mi", "eu", "ornare", "massa", "convallis", "nec", "Donec", "volutpat", "molestie", "velit", "scelerisque", "porttitor", "dui", "suscipit", "vel", 
"Etiam", "feugiat", "nisl", "vitae", "commodo", "ligula", "tristique", "nec", "Fusce", "bibendum", "fermentum", "rutrum"] 

Il = "Lorem ipsum dolor sit amet, consectetur adipisici elit "
Il_temp = "" 
diesisteinkomma = ","
Punkt = "."
Zeilenumbruch = "\n \n \n" 

while True:
	Absätze=input("Wie Viele Absätze? ") 

	try: 
	   int(Absätze) 
	except:
		print("FEHLER: Eingabe muss integer sein")
	else:
		break

def Satz():

	global Il
	global Il_temp 

	Gross = True 
	Gbuch = "" 
	länge = randint(6, 20) 

	for i in range(länge): 
		
		Komma=randint(1, 15) 
		wort = (choice(LoremIpsum)) 
		Il_temp += " "

		if Gross == True:
			Il_temp += wort[:1].upper()
			Il_temp += wort[1:]

			Gross = False 
		else:
			Il_temp += wort 

		if Komma==10: 
			Il_temp += diesisteinkomma 

	Il_temp += Punkt

def Absatz(): 

	global Il 
	global Il_temp 

	Sätze=randint(4, 14)

	for i in range(Sätze): 
		Satz() 
	
	Il += Il_temp[1:]
	

for i in range(int(Absätze)):
	Absatz()
	Il += Zeilenumbruch

print(Il)


while True:
	cb = input("Soll der Text in die Zwischenablage kopiert werden? (y/n) ")

	if cb == "y":
		copy(Il)
		print("Text in die Zwischenablage kopiert") 
		print("\n")
		break 

	elif cb == "n":
		print("Text nicht in die Zwischenablage kopiert")
		print("\n")
		break

	else:
		print("FEHLER: FALSCHE EINGABE")

	print("\n")
