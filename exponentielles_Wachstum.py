  # WICHTIG! Die Anleitungen (grau / grün, mit einem # davor) bitte befolgen. Keine Buchstaben bei den Variablen einsetzen, nichts als True / False bei jedes_ergebnis. Wenn Fertig, oben auf "run" drücken.  WICHTIG!: True bzw. False müssen immer am Anfang groß geschrieben werden!

t = 4 # Hier den t- Wert eintragen.
b = 7244 # Hier den Wert für B(0) eintragen
Pro = 0.045 # Hier die Steigung eingeben (z.B. 80% = 0.8; Minuszahl bei Verlust)
x = 0 # Hier nichts ändern
Endguthaben = 0 # Wenn nicht bekannt, 0 eingeben

jedes_ergebnis = True # Hier True zu False ändern, falls man nicht jedes zwischenergebnis haben will.

Gewinn = True # Hier True eintragen, wenn man eine exponentielle Zunahme hat, False, wenn man eine exponentielle Abnahme (False wenn man Endguthaben berechnen will)

t_herausfinden = False # Hier True eintragen, wenn man t herausfinden will.

alles_zusammen = False # Hier True eintragen

nachkommastellen = 2 # Hier die Anzahl an Nachkommastellen eingeben 

if t_herausfinden == True:
  t = 9999999999

if Gewinn == True:
    if Endguthaben < b:
        ("FEHLER: Wenn ein Gewinn gemacht wird, muss das Endguthaben größer als B(0) sein!")
elif Gewinn == False:
    if Endguthaben > b:
        raise Exception("FEHLER: Wenn ein Verlust gemacht wird gemacht wird, muss das Endguthaben kleiner als B(0) sein! ")

print("t = 0 :", b)

for i in range(t):

    if Gewinn == True:
        b += b * Pro
        x += b
    else:
        Endguthaben -= Endguthaben * Pro
        x += Endguthaben
    
    if t_herausfinden == True:
        if Gewinn == True:
            if b < Endguthaben:
                print("t =", int(i + 1), ":", round(b, nachkommastellen))
                print("t ist ungefähr", i + 1)
                break
        elif Gewinn == False:
            if b > Endguthaben:
                print("t =", int(i + 1), ":", round(b, nachkommastellen))
                print("t ist ungefähr", i + 1)
                break

    if jedes_ergebnis == True:
      print("t =", int(i + 1), ":", round(b, nachkommastellen))
      
if jedes_ergebnis == False:
  print("Ergebnis von t=", 13, ":", round(b, nachkommastellen))

if alles_zusammen == True:
  print("Alle Ergebnisse zusammengerechnet:", round(x, nachkommastellen))
